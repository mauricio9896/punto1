from tkinter import *
from PIL import Image
from PIL import ImageTk


cuenta=0
counter =0
mt=0
hr=0

root=Tk()
root.title("Estado del potenciometro")
root.geometry('400x400')
root.configure(bg='pale turquoise')


def estado_pot():
    global cuenta, counter,mt,hr
    counter += 1
    estado['text'] = str(counter)
    cuenta=estado.after(100,estado_pot)
    
    if (counter==60):
        counter=0
        mt +=1
        minutos['text'] = str(mt)
        if(mt==60):
            mt=0
            hr +=1
            horas['text'] = str(hr)
def pause():
    global cuenta
    estado.after_cancel(cuenta)

def reiniciar():
    global counter,mt,hr
    counter=0
    mt=0
    hr=0

#IMAGEN
width=int(150/2)
height=int(150/3)

img=Image.open("C:/Users/LENOVO/Desktop/herramientas/usa.png")#abre la imagen
img=img.resize((width,height))#se cambia el tamaño
imagen=ImageTk.PhotoImage(img) #almacena la imagen en un objeto
b=Label(root, image=imagen).place(x=300,y=10) #coloca la imagen en el objeto

###########################################################

titulo=Label(root,text="PUNTO 1",font=("Rockwell Extra Bold",18),fg="black")
titulo.pack()
titulo.configure(bg='pale turquoise')

estado=Label(root,text="00")
estado.place(x=230,y=90)
estado.config(bg='pale turquoise',width=6, height=2)

mitad=Label(root,text=":")
mitad.place(x=210,y=90)
mitad.config(bg='pale turquoise',width=2, height=2)

minutos=Label(root,text="00")
minutos.place(x=160,y=90)
minutos.config(bg='pale turquoise',width=6, height=2)

mitad1=Label(root,text=":")
mitad1.place(x=150,y=90)
mitad1.config(bg='pale turquoise',width=2, height=2)

horas=Label(root,text="00")
horas.place(x=100,y=90)
horas.config(bg='pale turquoise',width=6, height=2)

#####################################

start=Button(root,text="iniciar",command=estado_pot)
start.place(x=140,y=140)
start.config(width=15, height=2)

pause=Button(root,text="parar",command=pause)
pause.place(x=140,y=190)
pause.config(width=15, height=2)

reiniciar=Button(root,text="reiniciar",command=reiniciar)
reiniciar.place(x=140,y=240)
reiniciar.config(width=15, height=2)

stop=Button(root,text="salir",command=root.destroy)
stop.place(x=140,y=290)
stop.config(width=15, height=2)

root.mainloop()

